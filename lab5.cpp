/*******************
  Samuel Hagen
  shagen
  Lab 5
  Lab Section: 5
  TA: Alex Myers
*******************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

//Function prototypes
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  srand(unsigned (time(0)));

  Card deck[52];
  int i = 0;
  int j = 0;
  int count = 0;
  Card hand[5];

  //Deck Pointers
  Card *startPointer;
  Card *endPointer;
  //Function Pointer
  int (*myrandomPtr)(int) = myrandom;

  //Creates the deck of cards
  for (j = 0; j < 4; ++j) {
    for (i = 2; i <= 14; ++i) {
      deck[count].value = i;
      deck[count].suit = static_cast<Suit>(j);
      ++count;
    }
  }

  //Points to the start of deck and the address one past the end of the deck
  startPointer = deck;
  endPointer = &deck[52];

  //Shuffles using the deck pointers and the pointer to the myrandom function
  random_shuffle(startPointer, endPointer, myrandomPtr);

  //Draws 5 cards from the top of the deck
  for (i = 0; i < 5; ++i) {
    hand[i].value = deck[i].value;
    hand[i].suit = deck[i].suit;
  }

  //Hand pointers
  Card *handStartPtr = hand;
  Card *handEndPtr = &hand[5];
  //Sorts hand using hand pointers and suit_order function
  sort(handStartPtr, handEndPtr, suit_order);

  //Prints out the sorted hand
  for (i = 0; i < 5; ++i) {
    cout << setw(10) << get_card_name(hand[i]) << " of "
    << get_suit_code(hand[i]) << endl;
  }

  return 0;
}

/*Takes in two card values and first compares their suits. If needed, the
values of the cards are compared*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit) {
    return(true);
  }

  else if (lhs.suit == rhs.suit) {
    if (lhs.value < rhs.value) {
      return(true);
    }
    else {
      return(false);
    }
  }

  else {
    return(false);
  }
}

//Takes in a card and returns it's proper suit
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//Takes in a card and returns it's proper value or name
string get_card_name(Card& c) {
  switch (c.value) {
    case 2 :        return "2";
    case 3 :        return "3";
    case 4 :        return "4";
    case 5 :        return "5";
    case 6 :        return "6";
    case 7 :        return "7";
    case 8 :        return "8";
    case 9 :        return "9";
    case 10 :       return "10";
    case 11:        return "Jack";
    case 12:        return "Queen";
    case 13:        return "King";
    case 14:        return "Ace";
    default:        return "";
  }
}
